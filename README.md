**Install packages**
````
sudo pacman -S chromium firefox termite lsd imagewriter maven sbt scala stack haxe nim nimble julia go rust uncrustify zathura clang cmake clisp r nodejs npm rofi pstoedit libmythes beanshell postgresql-libs mariadb-libs coin-or-mp ed biber dialog perl-tk psutils python2-pygments libreoffice-fresh mono dotnet-runtime dotnet-sdk dotnet-host ocaml r erlang-unixodbc lksctp-tools texlive-most texlive-lang ruby-docs tk zathura-djvu zathura-pdf-poppler zathura-ps gnuplot scala-docs graphviz erlang elixir adobe-source-sans-pro-fonts adobe-source-serif-pro-fonts brave firefox ttf-font-awesome awesome-terminal-fonts powerline powerline-fonts ttf-font-icons ttf-fira-code ttf-fira-mono ttf-fira-sans
````

**Only if i3WM is required.**
````
sudo pacman -S i3 i3-dump-log i3-msg i3-sensible-pager i386 i3exit i3-nagbar i3-sensible-terminal i3bar i3-input i3-save-tree i3status i3-config-wizard i3lock i3-scrot i3-with-shmlog i3-dmenu-desktop i3-migrate-config-to-v4 i3-sensible-editor  

````
**Install Vundle Package Manager**

````
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

````
**Install Greenclip - Clipboard Manager**

````
yay -S rofi-greenclip gruvbox-icon-theme gtk-theme-arc-gruvbox-git
````