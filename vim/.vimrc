" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

let g:powerline_pycmd = 'py3'

" Some basics:
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber

" ================ No Swap ==========================
set noswapfile
set nobackup
set nowb

" ================ Indentation ======================
set autoindent
set smartindent
set smarttab
set tabstop=2       " The width of a TAB is set to 2.
                    " Still it is a \t. It is just that
                    " Vim will interpret it to be having
                    " a width of 2.

set shiftwidth=2    " Indents will have a width of 2

set softtabstop=2   " Sets the number of columns for a TAB

set expandtab       " Expand TABs to spaces

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden

" The mapleader has to be set before vundle starts loading all
" the plugins.
let mapleader = "\\"

" Mapping leader key to open NERDTree
nmap <leader>ne :NERDTree<cr>

"Always open with NERDTree
autocmd VimEnter * NERDTree

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.
if has('persistent_undo')
  silent !mkdir ~/.vim/backups > /dev/null 2>&1
  set undodir=~/.vim/backups
  set undofile
endif

" Auto indent pasted text
nnoremap p p=`]<C-o>
nnoremap P P=`]<C-o>

" Set font as FiraCode
set guifont='FiraCode'

" Enable autocompletion:
set wildmode=longest,list,full

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set runtimepath+=~/.vim/bundle/vim-addon-manager/

call vundle#begin()

  " let Vundle manage Vundle, required
  Plugin 'VundleVim/Vundle.vim'
  Plugin 'marcweber/vim-addon-manager'

  " Useful plugins
  Plugin 'scrooloose/nerdtree'
  Plugin 'tpope/vim-surround'
  Plugin 'PotatoesMaster/i3-vim-syntax'
  Plugin 'tpope/vim-fugitive'

  " Airline Themes
  Plugin 'vim-airline/vim-airline'
  Plugin 'vim-airline/vim-airline-themes'
  Plugin 'rafi/awesome-vim-colorschemes'

  " Cursors
  Plugin 'terryma/vim-multiple-cursors'

  " Markdown
  Plugin 'tpope/vim-markdown'

  " Language Pluginins
  Plugin 'valloric/youcompleteme'
  Plugin 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
  Plugin 'rust-lang/rust.vim'
  Plugin 'neovimhaskell/haskell-vim'
  Plugin 'leshill/vim-json'
  Plugin 'jparise/vim-graphql'
  Plugin 'udalov/kotlin-vim'
  Plugin 'tpope/vim-fireplace'
  Plugin 'rgrinberg/vim-ocaml'
  Plugin 'zah/nim.vim'
  Plugin 'derekwyatt/vim-scala'
  Plugin 'julialang/julia-vim'

  " Rails and Ruby Pluginins
  Plugin 'vim-ruby/vim-ruby'
  Plugin 'ecomba/vim-ruby-refactoring'
  Plugin 'tpope/vim-rails'
  Plugin 'tpope/vim-rake'

  " Erlang and Elixir Pluginins
  Plugin 'elixir-lang/vim-elixir'
  Plugin 'vim-erlang/vim-erlang-omnicomplete'
  Plugin 'vim-erlang/vim-erlang-compiler'

  " Javascript Pluginins
  Plugin 'burnettk/vim-angular'
  Plugin 'reasonml-editor/vim-reason'
  Plugin 'nono/jquery.vim'
  Plugin 'pangloss/vim-javascript'

  " LaTeX Pluginin
  Plugin 'lervag/vimtex'

  " Java Formatter
  Plugin 'google/vim-codefmt'

  " Others
  Plugin 'ekalinin/dockerfile.vim'
  Plugin 'othree/xml.vim'
call vundle#end()

" Loads glaive, vtd, and their maktaba dependency.
call vam#ActivateAddons(['glaive', 'vtd'])
" Initializes all maktaba plugins.
call maktaba#plugin#Detect()

" the glaive#Install() should go after the "call vundle#end()"
call glaive#Install()
" Optional: Enable codefmt's default mappings on the <Leader>= prefix.
Glaive codefmt plugin[mappings]
Glaive codefmt google_java_executable="java -jar /home/samanth/Downloads/Softwares/Utilities/google-java-format-1.7-all-deps.jar"

" Automatically deletes all trailing whitespace on save.
autocmd BufWritePre * %s/\s\+$//e

" Autoformat
augroup autoformat_settings
  autocmd FileType bzl AutoFormatBuffer buildifier
  autocmd FileType c,cpp,proto,javascript AutoFormatBuffer clang-format
  autocmd FileType dart AutoFormatBuffer dartfmt
  autocmd FileType go AutoFormatBuffer gofmt
  autocmd FileType gn AutoFormatBuffer gn
  autocmd FileType html,css,sass,scss,less,json AutoFormatBuffer js-beautify
  autocmd FileType java AutoFormatBuffer google-java-format
  autocmd FileType python AutoFormatBuffer yapf
  " Alternative: autocmd FileType python AutoFormatBuffer autopep8
  autocmd FileType rust AutoFormatBuffer rustfmt
  autocmd FileType vue AutoFormatBuffer prettier
augroup END

" Colorscheme
set background=dark
colorscheme gruvbox

" Set Airline Theme
let g:airline_theme='angr'
"let g:airline_theme='ubaryd'

if has("gui_running")
"tell the term has 256 colors
  set t_Co=256
end

" Better search
set hlsearch
set incsearch

if executable('rg')
  set grepprg=rg\ --vimgrep\ --no-heading
  set grepformat=%f:%l:%m
endif

set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points

set scrolloff=5 " Keep 5 lines below and above the cursor

set cursorline

set laststatus=2
set statusline=%f " tail of the filename
set statusline+=\ c:%c " column number
set statusline+=%= " switching to right side

command FormatJSON %!python -m json.tool

autocmd VimResized * wincmd = " Automatically resize splits when resizing window

"lazy js. Append ; at the end of the line
nnoremap <Leader>; m`A;<Esc>``

nmap <Leader>s :write<Enter>
nmap <Leader>r :redraw!<Enter>
